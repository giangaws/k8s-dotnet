﻿using Sengo.Model.Type;
using System;
using System.ComponentModel.DataAnnotations;

namespace Sengo.Model.Schedule
{
    public class ScheduleModel
    {
        public int Id { get; set; }

        public bool IsDeleted { get; set; }

        public string VersionNo { get; set; }

        public DateTime CreatedDate { get; set; }

        public string CreatedUser { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public string UpdatedUser { get; set; }

        [Display(Name = "Tên")]
        [Required(ErrorMessage = "Chưa nhập Tên")]
        [MaxLength(100, ErrorMessage = "Chiều dài Tên Schedule tối đa 100 ký tự")]
        public string Name { get; set; }

        [Display(Name = "Mô tả")]
        [MaxLength(1000, ErrorMessage = "Chiều dài Mô tả Schedule tối đa 1000 ký tự")]
        public string Description { get; set; }

        [Display(Name = "Thời gian")]
        [Required(ErrorMessage = "Chưa nhập Thời gian")]
        [MaxLength(100, ErrorMessage = "Chiều dài Thời gian tối đa 100 ký tự")]
        public string Cron { get; set; }

        [Display(Name = "Thao tác")]
        [Required(ErrorMessage = "Chưa nhập Thao tác")]
        public ScheduleAction Action { get; set; }

        [Display(Name = "Active")]
        public bool IsActive { get; set; }
    }
}
