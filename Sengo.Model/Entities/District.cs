﻿namespace Sengo.Model.Entities
{
    public class District : BaseEntity
    {
        public string DistrictCityName { get; set; }

        public string DistrictCode { get; set; }

        public int DisplayOrder { get; set; }

        public long CityId { get; set; }
    }
}
