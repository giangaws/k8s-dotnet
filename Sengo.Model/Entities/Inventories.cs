﻿namespace Sengo.Model.Entities
{
    public class Inventories : BaseEntity
    {
        public int? LocationId { get; set; }

        public int? StoreId { get; set; }

        public int? ProductId { get; set; }

        public int? UnavailableQuantity { get; set; }

        public int? PhysicalQuantity { get; set; }

        public int? AvailableQuantity { get; set; }

        public int? Total { get; set; }

        public int? ProductIdExternal { get; set; }
    }
}
