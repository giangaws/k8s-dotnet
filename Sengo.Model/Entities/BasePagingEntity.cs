﻿using System;

namespace Sengo.Model.Entities
{
    public class BasePagingEntity
    {
        public int PageSize { get; set; }

        public int PageNumber { get; set; }

        public string SortColumn { get; set; }

        public string SortOrder { get; set; }

        public DateTime FromDate { get; set; }

        public DateTime ToDate { get; set; }

        public int Total { get; set; }
    }
}
