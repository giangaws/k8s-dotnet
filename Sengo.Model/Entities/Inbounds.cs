﻿using System;

namespace Sengo.Model.Entities
{
    public class Inbounds : BaseEntity
    {
        public string InboundCode { get; set; }

        public int? StoreId { get; set; }

        public string RefCode { get; set; }

        public string InvoiceCode { get; set; }

        public string Deliveryer { get; set; }

        public DateTime? DeliveryDate { get; set; }

        public int? LocationId { get; set; }

        public string Note { get; set; }

        public int? InboundStatus { get; set; }

        public string Description { get; set; }

        public int? DeliveryType { get; set; }// hình thức lấy hàng 1:Pickup 2: DropOff

        public string DeliveryImportFile { get; set; }

        public string DeliveryerIdCard { get; set; }

        public int? Carrier { get; set; }
    }
}
