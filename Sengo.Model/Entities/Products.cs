﻿namespace Sengo.Model.Entities
{
    public class Products : BaseEntity
    {
        public string ProductCode { get; set; }

        public string ProductName { get; set; }

        public int? UnitType { get; set; }

        public int? CategoryType { get; set; }

        public int? StoreId { get; set; }

        public int? LocationId { get; set; }

        public int? Length { get; set; }

        public int? Width { get; set; }

        public int? Height { get; set; }

        public string Description { get; set; }

        public int ProductIdExternal { get; set; }

        public string GCode { get; set; }

        public string Link { get; set; }

        public int? Weight { get; set; }
    }
}
