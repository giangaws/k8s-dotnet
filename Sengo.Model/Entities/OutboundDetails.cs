﻿namespace Sengo.Model.Entities
{
    public class OutboundDetails : BaseEntity
    {
        public int? OutboundId { get; set; }

        public int? ProductId { get; set; }

        public int? Quantity { get; set; }

        public int? Weight { get; set; }

        public int? Length { get; set; }

        public int? Width { get; set; }

        public int? Height { get; set; }

        public decimal? Price { get; set; }

        public decimal? SubTotal { get; set; }
    }
}
