﻿namespace Sengo.Model.Entities
{
    public class RoleFeatures : BaseEntity
    {
        public int? RoleId { get; set; }

        public int? Feature { get; set; }

        public bool Enable { get; set; }
    }
}
