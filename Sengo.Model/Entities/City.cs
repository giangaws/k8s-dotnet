﻿namespace Sengo.Model.Entities
{
    public class City : BaseEntity
    {
        public string CityName { get; set; }

        public string ZipCode { get; set; }

        public string CityCode { get; set; }

        public int DisplayOrder { get; set; }
    }
}
