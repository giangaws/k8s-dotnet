﻿namespace Sengo.Model.Entities
{
    public class Warehouses : BaseEntity
    {
        public string WarehouseCode { get; set; }

        public string WarehouseName { get; set; }
    }
}
