﻿namespace Sengo.Model.Entities
{
    public class Ward : BaseEntity
    {
        public string WardName { get; set; }

        public int DisplayOrder { get; set; }

        public long DistrictId { get; set; }
    }
}
