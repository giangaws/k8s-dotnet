﻿namespace Sengo.Model.Entities
{
    public class Outbounds : BaseEntity
    {
        public int StoreId { get; set; }

        public string OutboundCode { get; set; }

        public string OrderNumber { get; set; }

        public string RefCode { get; set; }

        public int LocationId { get; set; }

        public string CarrierCode { get; set; }

        public string CarrierName { get; set; }

        public decimal CodAmount { get; set; }

        public string TrackingNumber { get; set; }

        public int PaymentMethod { get; set; }

        public string Note { get; set; }

        public string ReceiverName { get; set; }

        public string ReceiverPhone { get; set; }

        public string ReceiverAddress { get; set; }

        public string SenderName { get; set; }

        public string SenderPhone { get; set; }

        public string SenderAdress { get; set; }

        public int ShipFromCityId { get; set; }

        public string ShipFromCityName { get; set; }

        public int ShipFromDistrictId { get; set; }

        public string ShipFromDistrictName { get; set; }

        public int ShipFromWardId { get; set; }

        public string ShipFromWardName { get; set; }

        public int ShipToCityId { get; set; }

        public string ShipToCityName { get; set; }

        public int ShipToDistrictId { get; set; }

        public string ShipToDistrictName { get; set; }

        public int ShipToWardId { get; set; }

        public string ShipToWardName { get; set; }

        public int ServicePackge { get; set; }

        public int OutboundStatus { get; set; }
    }
}
