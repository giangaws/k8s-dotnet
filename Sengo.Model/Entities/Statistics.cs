﻿namespace Sengo.Model.Entities
{
    public class Statistics
    {
        public int? TotalIBToDay { get; set; }

        public int? TotalIBCancleToDay { get; set; }

        public int? TotalOBToDay { get; set; }

        public int? TotalOBCancleToDay { get; set; }
    }
}
