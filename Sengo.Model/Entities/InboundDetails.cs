﻿namespace Sengo.Model.Entities
{
    public class InboundDetails : BaseEntity
    {
        public int? InboundId { get; set; }

        public int? ProductId { get; set; }

        public int? Quantity { get; set; }

        public decimal? Price { get; set; }

        public decimal? PricePromotion { get; set; }
    }
}
