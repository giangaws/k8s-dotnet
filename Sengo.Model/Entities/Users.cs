﻿using System;

namespace Sengo.Model.Entities
{
    public class Users : BaseEntity
    {
        public int? RoleId { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public string FullName { get; set; }

        public int? Type { get; set; }

        public int? Status { get; set; }

        public string Avatar { get; set; }

        public DateTime? LastLoginDate { get; set; }

        public string Password { get; set; }

        public Guid? Salt { get; set; }
    }
}
