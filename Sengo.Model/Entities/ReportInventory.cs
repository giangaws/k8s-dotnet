﻿namespace Sengo.Model.Entities
{
    public class ReportInventory : BaseEntity
    {
        public int? StoreId { get; set; }

        public int? ProductId { get; set; }

        public int? LocationId { get; set; }

        public int? Period { get; set; }

        public int? FirstQuantity { get; set; }

        public int? LastQuantity { get; set; }
    }
}
