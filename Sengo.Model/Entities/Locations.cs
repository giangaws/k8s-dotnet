﻿namespace Sengo.Model.Entities
{
    public class Locations : BaseEntity
    {
        public string LocationName { get; set; }

        public int? LocationType { get; set; }

        public string Address { get; set; }

        public int? WarehouseId { get; set; }
    }
}
