﻿using System.Net;

namespace Sengo.Model.BaseReponse
{
    public class IntegrationResponse
    {
        public HttpStatusCode Status { get; set; }

        public string Message { get; set; }

    }

    public class IntegrationResponse<T>
    {
        public HttpStatusCode Status { get; set; }

        public string Message { get; set; }

        public T Data { get; set; }

    }
}
