﻿using System;
using System.Collections.Generic;

namespace Sengo.Model.BaseReponse
{
    public class BaseResponse<T> : BaseResponse
    {
        public BaseResponse()
        {
            Success = true;
            Messages = new List<string>();
            Data = default;
        }

        public BaseResponse(bool success, string message)
        {
            Success = success;
            if (!string.IsNullOrWhiteSpace(message))
            {
                Messages.Add(message);
            }
            Data = default;
        }

        public BaseResponse(bool success, List<string> messages)
        {
            Success = success;
            Messages = messages;
            Data = default;
        }

        public BaseResponse(bool success, string message, T data)
        {
            Success = success;

            if (!string.IsNullOrWhiteSpace(message))
            {
                Messages.Add(message);
            }
            Data = data;
        }

        public BaseResponse(bool success, List<string> messages, T data)
        {
            Success = success;
            Messages = messages;
            Data = data;
        }

        public T Data { get; set; }
    }

    [Serializable]
    public class BaseResponse
    {
        public BaseResponse()
        {
            Success = true;

            Messages = new List<string>();
        }

        public BaseResponse(bool success, string message = "") : this()
        {
            Success = success;

            if (!string.IsNullOrWhiteSpace(message))
            {
                Messages.Add(message);
            }
        }

        public BaseResponse(bool success, List<string> messages) : this()
        {
            Success = success;

            if (messages != null && messages.Count > 0)
            {
                Messages.AddRange(messages);
            }
        }

        public bool Success { get; set; }

        public List<string> Messages { get; set; }

        public static BaseResponse FailResponse(string message)
        {
            return new BaseResponse(false, message);
        }

        public static BaseResponse SuccessResponse(string message = null)
        {
            return new BaseResponse(true, message);
        }

        public void CheckSuccess()
        {
            Success = Messages.Count == 0;
        }

        public void AddMessage(string message)
        {
            Messages.Add(message);
        }

        public BaseResponse<T> ToBaseResponse<T>()
        {
            return new BaseResponse<T>(Success, Messages, default);
        }
    }
}
