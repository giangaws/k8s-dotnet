﻿using System.Collections.Generic;

namespace Sengo.Model.BaseReponse
{
    public class BaseFindResponse<T> where T : class
    {
        public BaseFindResponse()
        {
            Results = new List<T>();
        }

        public List<T> Results { get; set; }

        public int TotalRecords { get; set; }
    }
}
