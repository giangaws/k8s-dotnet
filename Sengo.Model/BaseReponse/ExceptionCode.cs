﻿namespace Sengo.Model.BaseReponse
{
    public class ExceptionCode
    {
        public static readonly int OK = 200;
        public static readonly int ErrorCode = 404;
        public static readonly int ErrorException = 500;
    }
}
