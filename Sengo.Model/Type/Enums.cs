﻿using System.ComponentModel;

namespace Sengo.Model.Type
{
    public enum RouteType
    {
        [Description("Tất cả")]
        AllRoute = 0,
        [Description("Intra Metro")]
        IntraMetro = 1,
        [Description("Cross Metro")]
        CrossMetro = 2,
        [Description("Same Province")]
        SameProvince = 3,
        [Description("Same Region")]
        SameRegion = 4,
        [Description("Cross Region")]
        CrossRegion = 5,
        [Description("Nearby Region")]
        NearbyRegion = 6,
        [Description("Special Lane 1")]
        SpecialLane1 = 7,
        [Description("Special Lane 2")]
        SpecialLane2 = 8

    }
    public enum ShopLevel
    {

        [Description("Tất cả")]
        AllLevel = 0,

        [Description("Level 1")]
        Level1 = 1,

        [Description("Level 2")]
        Level2 = 2,

        [Description("Level 3")]
        Level3 = 3,
    }
    public enum DiscountType
    {
        [Description("Giảm giá %")]
        Percent = 1,

        [Description("Giảm giá tiền")]
        DiscountAmount = 2,

        [Description("Đồng giá")]
        FixedPrice = 3,
    }

    public enum EnumFeatureGroup
    {
        [Description("Người dùng")]
        Users = 11,
        [Description("Phân quyền")]
        Roles = 12,
        [Description("Đơn hàng")]
        Order = 13,
        [Description("Giảm giá PVC")]
        Discount = 14,
        [Description("Hệ thống")]
        System = 15,
        [Description("Cấp bậc shop")]
        ShopLevel = 16,
        [Description("Cấp bậc")]
        Level = 17,
        [Description("Điều phối")]
        Dispatch = 18,
    }

    public enum EnumRoleFeatures
    {
        [Description("Người dùng - Xem người dùng")]
        Users_View = 1101,
        [Description("Người dùng - Thêm người dùng")]
        Users_Add = 1102,
        [Description("Người dùng - Cập nhật người dùng")]
        Users_Update = 1103,
        [Description("Người dùng - Xóa người dùng")]
        Users_Delete = 1104,

        [Description("Phân quyền - Xem phân quyền")]
        Roles_View = 1201,
        [Description("Phân quyền - Thêm phân quyền")]
        Roles_Add = 1202,
        [Description("Phân quyền - Cập nhật phân quyền")]
        Roles_Update = 1203,
        [Description("Phân quyền - Xóa phân quyền")]
        Roles_Delete = 1204,

        [Description("Đơn hàng - Xem đơn hàng")]
        Order_View = 1301,
        [Description("Đơn hàng - Xuất đơn hàng")]
        Order_Export = 1305,

        [Description("Giảm giá PVC- Xem Giảm giá PVC")]
        Discount_View = 1401,
        [Description("Giảm giá PVC- Thêm Giảm giá PVC")]
        Discount_Add = 1402,
        [Description("Giảm giá PVC- Cập nhật Giảm giá PVC")]
        Discount_Update = 1403,
        [Description("Giảm giá PVC- Xóa Giảm giá PVC")]
        Discount_Delete = 1404,

        [Description("Hệ thống - Xem hệ thống")]
        System_View = 1501,

        [Description("Cấp bậc shop - Xem Cấp bậc shop")]
        ShopLevel_View = 1601,
        [Description("Cấp bậc shop - Thêm Cấp bậc shop")]
        ShopLevel_Add = 1602,
        [Description("Cấp bậc shop - Cập nhật Cấp bậc shop")]
        ShopLevel_Update = 1603,
        [Description("Cấp bậc shop - Xóa Cấp bậc shop")]
        ShopLevel_Delete = 1604,

        [Description("Cấp bậc - Xem Cấp bậc")]
        Level_View = 1701,
        [Description("Cấp bậc - Thêm Cấp bậc")]
        Level_Add = 1702,
        [Description("Cấp bậc - Cập nhật Cấp bậc")]
        Level_Update = 1703,
        [Description("Cấp bậc - Xóa Cấp bậc")]
        Level_Delete = 1704,

        [Description("Điều phối - Xem Điều phối")]
        Dispatch_View = 1801,
    }

    public enum ScheduleAction
    {
        [Description("Vận đơn")]
        Tracking = 11,
        [Description("Phí vận chuyển")]
        PVC = 21,
        [Description("Retry Queue")]
        RetryQueue = 31
    }
}
