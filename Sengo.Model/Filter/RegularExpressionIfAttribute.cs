﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Sengo.Model.Filter
{
    public class RegularExpressionIfAttribute : ConditionalValidationAttribute
    {
        private readonly string pattern;

        protected override string ValidationName
        {
            get { return "regularexpressionif"; }
        }

        public RegularExpressionIfAttribute(string pattern, string dependentProperty, object targetValue)
            : base(new RegularExpressionAttribute(pattern), dependentProperty, targetValue)
        {
            this.pattern = pattern;
        }

        protected override IDictionary<string, object> GetExtraValidationParameters()
        {
            return new Dictionary<string, object>
            {
                {"rule", "regex"},
                {"ruleparam", pattern }
            };
        }
    }
}
