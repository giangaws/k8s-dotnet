﻿using System;

namespace Sengo.Model.Filter
{
    public class BasePaging<T>
    {
        public int PageSize { get; set; }

        public int PageNumber { get; set; }

        public string SortColumn { get; set; }

        public string SortOrder { get; set; }

        public DateTime FromDate { get; set; }

        public DateTime ToDate { get; set; }

        public int Total { get; set; }

        public T Model { get; set; }
    }
}
