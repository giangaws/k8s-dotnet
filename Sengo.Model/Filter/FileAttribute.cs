﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Sengo.Model.Filter
{
    public class FileAttribute : ValidationAttribute
    {
        public int MaxContentLength = int.MaxValue;
        //public string[] AllowedFileExtensions;

        public override bool IsValid(object value)
        {
            var contentLength = 0;

            if (!(value is List<IFormFile> files))
            {
                return true;
            }

            foreach (var item in files)
            {
                contentLength += Convert.ToInt32(item.Length);
            }

            if (contentLength > MaxContentLength)
            {
                ErrorMessage = String.Format("File is too large, maximum allowed is: {0} MB", (MaxContentLength / 1024));

                return false;
            }

            return true;
        }
    }
}
