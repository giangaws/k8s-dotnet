﻿namespace Sengo.Model.Pub
{
    public class WebPubDefineEvents
    {
        public static readonly string UpdateQualityProduct = "fulfillment.product.update";
        public static readonly string UpdateOutbound = "fulfillment.outbound.update";
        public static readonly string UpdateInbound = "fulfillment.inbound.update";
    }
}
