﻿using Sengo.Model.Entities;
using Sengo.Model.Filter;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Sengo.Model.Resquest
{
    public class InboundRequest : BaseEntity
    {
        public string WareHouseCode { get; set; }

        public string InboundCode { get; set; }

        [Required(ErrorMessage = "Vui lòng chọn cửa hàng")]
        public int? StoreId { get; set; }

        public string RefCode { get; set; }

        public string InvoiceCode { get; set; }

        [RequiredIf("DeliveryType", 2, ErrorMessage = "Vui lòng nhập người giao hàng")]
        public string Deliveryer { get; set; }

        public DateTime? DeliveryDate { get; set; }

        [Required(ErrorMessage = "Vui lòng chọn loại hàng")]
        public int? LocationId { get; set; }

        public string Note { get; set; }

        public int? InboundStatus { get; set; }

        public string Description { get; set; }

        public string RefStatus { get; set; }

        public DateTime? RefStatusDate { get; set; }

        public string Comment { get; set; }

        public string Status { get; set; }

        public DateTime DateUpdate { get; set; }

        public List<InboundDetailRequest> InboundDetails { get; set; }

        public string DeliveryImportFile { get; set; }

        [Required(ErrorMessage = "Vui lòng chọn hình thức lấy hàng")]
        public int? DeliveryType { get; set; }// hình thức lấy hàng 1:Pickup 2: DropOff

        [RequiredIf("DeliveryType", 2, ErrorMessage = "Vui lòng nhập CMND")]
        public string DeliveryerIdCard { get; set; }

        [RequiredIf("DeliveryType", 1, ErrorMessage = "Vui lòng chọn nhà vận chuyển")]
        public int? Carrier { get; set; }

        [File(MaxContentLength = 1024 * 1024 * 5, ErrorMessage = "File không vượt quá 5MB")]
        [IgnoreDataMember]
        public IEnumerable<IFormFile> ImportFiles { get; set; }

        [File(MaxContentLength = 1024 * 1024 * 5, ErrorMessage = "File không vượt quá 5MB")]
        [IgnoreDataMember]
        public IEnumerable<IFormFile> ImportFilesProduct { get; set; }
    }
}
