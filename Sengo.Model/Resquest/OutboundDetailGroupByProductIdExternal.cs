﻿namespace Sengo.Model.Resquest
{
    public class OutboundDetailGroupByProductIdExternal
    {
        public int ProductIdExternal { get; set; }

        public int Quantity { get; set; }
    }
}
