﻿using Sengo.Model.Entities;
using System.ComponentModel.DataAnnotations;

namespace Sengo.Model.Resquest
{
    public class ProductRequest : BaseEntity
    {
        public string ProductCode { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập tên sản phẩm")]
        public string ProductName { get; set; }

        [Required(ErrorMessage = "Vui lòng chọn đơn vị tính")]
        public int? UnitType { get; set; }

        public int? CategoryType { get; set; }

        [Required(ErrorMessage = "Vui lòng chọn cửa hàng")]
        public int? StoreId { get; set; }

        [Required(ErrorMessage = "Vui lòng chọn loại hàng")]
        public int? LocationId { get; set; }

        public int? Length { get; set; }

        public int? Width { get; set; }

        public int? Height { get; set; }

        public string Description { get; set; }

        [Required(ErrorMessage = "Vui lòng chọn SCode")]
        public int ProductIdExternal { get; set; }

        public string GCode { get; set; }

        public string Link { get; set; }

        public int? Weight { get; set; }
    }
}
