﻿namespace Sengo.Model.Resquest
{
    public class InboundSearchRequest : BasePagingRequest
    {
        public string InboundCode { get; set; }

        public int? InboundStatus { get; set; }

        public int? LocationId { get; set; }

        public int? WarehouseId { get; set; }

        public int? LocationType { get; set; }

        public string DateForCreate { get; set; }

        public string DateForDelivery { get; set; }

        public string StoreName { get; set; }

        public int? StoreId { get; set; }

        public string FromDeliveryDate { get; set; }

        public string ToDeliveryDate { get; set; }
    }
}