﻿namespace Sengo.Model.Resquest
{
    public class OutboundRequestCancel
    {
        public int Id { get; set; }

        public string OrderNumber { get; set; }
    }
}
