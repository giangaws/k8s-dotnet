﻿using System;

namespace Sengo.Model.Resquest
{
    public class CancelSalesOrderModel
    {
        public string OrderNumber { get; set; }

        /// <summary>
        /// Ngày hủy
        /// </summary>
        public DateTime? CancelDate { get; set; }

        /// <summary>
        /// Mã Lý do hủy
        /// </summary> 
        public string ReasonCancelCode { get; set; }

        /// <summary>
        /// Lý do hủy
        /// </summary> 
        public string CancelReason { get; set; }

        /// <summary>
        /// User hủy
        /// </summary> 
        public string CanceledBy { get; set; }
    }
}
