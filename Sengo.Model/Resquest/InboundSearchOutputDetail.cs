﻿using System;

namespace Sengo.Model.Resquest
{
    public class InboundSearchOutputDetail
    {
        public decimal? Price { get; set; }

        public int ProductIdExternal { get; set; }

        public int? Quantity { get; set; }

        public string ProductName { get; set; }

        public string RefCode { get; set; }

        public int Id { get; set; }

        public string InboundCode { get; set; }

        public string Deliveryer { get; set; }

        public string StoreName { get; set; }

        public string StoreCode { get; set; }

        public DateTime DeliveryDate { get; set; }

        public int? InboundStatus { get; set; }

        public DateTime CreatedDate { get; set; }

        public int? LocationType { get; set; }

        public string LocationName { get; set; }

        public string Description { get; set; }

        public string Note { get; set; }

        public int? DeliveryType { get; set; }// hình thức lấy hàng 1:Pickup 2: DropOff

        public string DeliveryImportFile { get; set; }

        public string DeliveryerIdCard { get; set; }

        public int? Carrier { get; set; }
    }
}
