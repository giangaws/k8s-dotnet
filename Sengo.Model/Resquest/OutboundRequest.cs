﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Sengo.Model.Resquest
{
    public class OutboundRequest
    {
        public int Id { get; set; }

        public DateTime CreatedDate { get; set; }

        public string CreatedUser { get; set; }

        public string UpdatedUser { get; set; }

        public DateTime UpdatedDate { get; set; }

        public bool IsDeleted { get; set; }

        public byte[] VersionNo { get; set; }

        [Required(ErrorMessage = "Vui lòng chọn cửa hàng")]
        public int StoreId { get; set; }

        public string OutboundCode { get; set; }

        public string OrderNumber { get; set; }

        public string RefCode { get; set; }

        [Required(ErrorMessage = "Vui lòng chọn loại hàng")]
        public int LocationId { get; set; }

        public string CarrierCode { get; set; }

        public string CarrierName { get; set; }

        public decimal CodAmount { get; set; }

        public string TrackingNumber { get; set; }

        public int PaymentMethod { get; set; }

        public string Note { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập tên người nhận")]
        public string ReceiverName { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập số điện thoại người nhận")]
        public string ReceiverPhone { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập địa chỉ người nhận")]
        public string ReceiverAddress { get; set; }

        public string SenderName { get; set; }

        public string SenderPhone { get; set; }

        public string SenderAdress { get; set; }

        public int ShipFromCityId { get; set; }

        public string ShipFromCityName { get; set; }

        public int ShipFromDistrictId { get; set; }

        public string ShipFromDistrictName { get; set; }

        public int ShipFromWardId { get; set; }

        public int ShipFromWardName { get; set; }

        [Required(ErrorMessage = "Vui lòng chọn thành phố")]
        public int ShipToCityId { get; set; }

        public string ShipToCityName { get; set; }

        [Required(ErrorMessage = "Vui lòng chọn quận/huyện")]
        public int ShipToDistrictId { get; set; }

        public string ShipToDistrictName { get; set; }

        [Required(ErrorMessage = "Vui lòng chọn phường/xã")]
        public int ShipToWardId { get; set; }

        public string ShipToWardName { get; set; }

        public int ServicePackge { get; set; }

        public int OutboundStatus { get; set; }

        public string RefStatus { get; set; }

        public DateTime RefStatusDate { get; set; }

        public string Comment { get; set; }

        public List<OutboundDetailRequest> OutboundDetails { get; set; }
    }
}
