﻿namespace Sengo.Model.Resquest
{
    public class OutboundDetailSearchOutput
    {
        public int Id { get; set; }

        public int ProductId { get; set; }

        public string ProductName { get; set; }

        public string ProductCode { get; set; }

        public int ProductIdExternal { get; set; }

        public int UnitType { get; set; }

        public int Quantity { get; set; }

        public string GCode { get; set; }

        public string StoreName { get; set; }

        public int StoreId { get; set; }
    }
}
