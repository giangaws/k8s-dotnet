﻿using Sengo.Model.Entities;
using System;

namespace Sengo.Model.Resquest
{
    public class InventoryStatusHistoryRequest : BaseEntity
    {
        public int? InventoryId { get; set; }

        public int? Status { get; set; }

        public string RefStatus { get; set; }

        public DateTime? RefStatusDate { get; set; }

        public string Comment { get; set; }
    }
}
