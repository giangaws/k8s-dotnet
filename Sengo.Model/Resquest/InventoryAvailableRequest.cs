﻿namespace Sengo.Model.Resquest
{
    public class InventoryAvailableRequest
    {
        public long ProductId { get; set; }

        public int LocationId { get; set; }
    }
}
