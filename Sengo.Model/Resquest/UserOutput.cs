﻿using System.Collections.Generic;

namespace Sengo.Model.Resquest
{
    public class UserOutput
    {
        public int Id { get; set; }

        public int RoleId { get; set; }

        public string Email { get; set; }

        public string FullName { get; set; }

        public List<RoleFeatureOutput> RoleFeatures { get; set; }
    }
}
