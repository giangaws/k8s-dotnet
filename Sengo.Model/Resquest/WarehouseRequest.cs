﻿using Sengo.Model.Entities;

namespace Sengo.Model.Resquest
{
    public class WarehouseRequest : BaseEntity
    {
        public string WarehouseCode { get; set; }

        public string WarehouseName { get; set; }
    }
}
