﻿using Sengo.Model.Entities;
using System.ComponentModel.DataAnnotations;

namespace Sengo.Model.Resquest
{
    public class RoleRequest : BaseEntity
    {
        [Required(ErrorMessage = "Vui lòng nhập tên nhóm quyền")]
        public string Name { get; set; }

        public string Description { get; set; }
    }
}
