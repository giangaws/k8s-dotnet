﻿using System;

namespace Sengo.Model.Resquest
{
    public class UserSearchOutput
    {
        public int Id { get; set; }

        public int RoleId { get; set; }

        public string RoleName { get; set; }

        public string Email { get; set; }

        public string FullName { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
