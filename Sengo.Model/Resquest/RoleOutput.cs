﻿using System.Collections.Generic;

namespace Sengo.Model.Resquest
{
    public class RoleOutput
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public List<RoleFeatureOutput> RoleFeatures { get; set; }
    }
}
