﻿namespace Sengo.Model.Resquest
{
    public class BasePagingRequest
    {
        public int PageSize { get; set; }

        public int PageNumber { get; set; }

        public string SortColumn { get; set; }

        public string SortOrder { get; set; }

        public string FromDate { get; set; }

        public string ToDate { get; set; }

        public int Total { get; set; }
    }
}
