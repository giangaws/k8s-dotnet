﻿namespace Sengo.Model.Resquest
{
    public class ProductSearchRequest : BasePagingRequest
    {
        public string ProductCode { get; set; }

        public string ProductName { get; set; }

        public int? CategoryType { get; set; }

        public string StoreName { get; set; }

        public int? LocationId { get; set; }

        public int? WarehouseId { get; set; }

        public string GCode { get; set; }

        public string DateFull { get; set; }

        public int? LocationType { get; set; }

        public int? ProductIdExternal { get; set; }

        public int? StoreId { get; set; }
    }
}
