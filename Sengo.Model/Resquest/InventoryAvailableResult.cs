﻿namespace Sengo.Model.Resquest
{
    public class InventoryAvailableResult
    {
        public int? ProductId { get; set; }

        public int? UnavailableQuantity { get; set; }

        public int? PhysicalQuantity { get; set; }

        public int? AvailableQuantity { get; set; }
    }
}
