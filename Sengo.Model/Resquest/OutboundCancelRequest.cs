﻿namespace Sengo.Model.Resquest
{
    public class OutboundCancelRequest
    {
        public int Id { get; set; }

        public string OrderNumber { get; set; }
    }
}