﻿using System;

namespace Sengo.Model.Entitys
{
    public class StatusOutboundResquest
    {
        public string RefCode { get; set; }

        public string Status { get; set; }

        public DateTime? DateUpdate { get; set; }

        public string RefStatus { get; set; }

        public DateTime? RefStatusDate { get; set; }

        public string Comment { get; set; }
    }
}
