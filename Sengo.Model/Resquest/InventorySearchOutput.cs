﻿namespace Sengo.Model.Resquest
{
    public class InventorySearchOutput
    {
        public int Id { get; set; }

        public int? CategoryType { get; set; }

        public string ProductName { get; set; }

        public string GCode { get; set; }

        public string StoreName { get; set; }

        public string StoreCode { get; set; }

        public int? StoreId { get; set; }

        public int? ProductId { get; set; }

        public int? UnavailableQuantity { get; set; }

        public int? PhysicalQuantity { get; set; }

        public int? AvailableQuantity { get; set; }

        public int? ProductIdExternal { get; set; }

        public int? LocationId { get; set; }

        public string LocationName { get; set; }

        public int? LocationType { get; set; }

        public bool? IsActive { get; set; }

        public bool? IsSold { get; set; }

        public bool? IsStock { get; set; }
    }
}