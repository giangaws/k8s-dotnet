﻿namespace Sengo.Model.Resquest
{
    public class UserSearchRequest : BasePagingRequest
    {
        public int RoleId { get; set; }

        public string Email { get; set; }

        public string FullName { get; set; }

        public string DateFull { get; set; }
    }
}
