﻿namespace Sengo.Model.Resquest
{
    public class RoleFeatureOutput
    {
        public int Id { get; set; }

        public int? RoleId { get; set; }

        public int? Feature { get; set; }

        public bool Enable { get; set; }
    }
}
