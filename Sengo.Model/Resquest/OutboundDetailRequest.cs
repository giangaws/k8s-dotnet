﻿using Sengo.Model.Entities;
using System.ComponentModel.DataAnnotations;

namespace Sengo.Model.Resquest
{
    public class OutboundDetailRequest : BaseEntity
    {
        public int? OutboundId { get; set; }

        [Required(ErrorMessage = "Vui lòng chọn sản phẩm")]
        public int? ProductId { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập số lượng")]
        public int? Quantity { get; set; }

        public int? Weight { get; set; }

        public int? Length { get; set; }

        public int? Width { get; set; }

        public int? Height { get; set; }

        public decimal? Price { get; set; }

        public decimal? SubTotal { get; set; }
    }
}
