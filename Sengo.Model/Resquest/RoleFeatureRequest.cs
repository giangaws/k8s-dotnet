﻿using Sengo.Model.Entities;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Sengo.Model.Resquest
{
    public class RoleFeatureRequest : BaseEntity
    {
        [Required(ErrorMessage = "Vui lòng chọn nhóm quyền")]
        public int? RoleId { get; set; }

        public int? Feature { get; set; }

        public bool Enable { get; set; }

        public List<RoleFeatures> RoleFeatures { get; set; }
    }
}
