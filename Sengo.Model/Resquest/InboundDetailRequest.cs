﻿using Sengo.Model.Entities;
using System.ComponentModel.DataAnnotations;

namespace Sengo.Model.Resquest
{
    public class InboundDetailRequest : BaseEntity
    {
        public int? InboundId { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập chọn sản phẩm")]
        public int? ProductId { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập chọn số lượng")]
        public int? Quantity { get; set; }

        public decimal? Price { get; set; }

        public decimal? PricePromotion { get; set; }
    }
}
