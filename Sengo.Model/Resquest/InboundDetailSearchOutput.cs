﻿using Sengo.Model.Entities;

namespace Sengo.Model.Resquest
{
    public class InboundDetailSearchOutput : InboundDetails
    {
        public string ProductName { get; set; }

        public string ProductCode { get; set; }

        public int ProductIdExternal { get; set; }

        public int UnitType { get; set; }

        public string GCode { get; set; }

        public string StoreName { get; set; }

        public int StoreId { get; set; }

        //public decimal? Price { get; set; } aready defined in InboundDetails

        //public decimal? PricePromotion { get; set; } aready defined in InboundDetails
    }
}
