﻿using System;
using System.Collections.Generic;

namespace Sengo.Model.Resquest
{
    public class OutboundSearchOutput
    {
        public string RefCode { get; set; }

        public int Id { get; set; }

        public int LocationId { get; set; }

        public string OutboundCode { get; set; }

        public string StoreName { get; set; }

        public string OrderNumber { get; set; }

        public string TrackingNumber { get; set; }

        public string StoreCode { get; set; }

        public string LocationName { get; set; }

        public int? LocationType { get; set; }

        public int? OutboundStatus { get; set; }

        public DateTime CreatedDate { get; set; }

        public string ReceiverName { get; set; }

        public string ReceiverAddress { get; set; }

        public string Note { get; set; }

        public string ShipToCityName { get; set; }

        public string ShipToDistrictName { get; set; }

        public string ShipToWardName { get; set; }

        public List<OutboundDetailSearchOutput> OutboundDetails { get; set; }
    }
}
