﻿using System;

namespace Sengo.Model.Resquest
{
    public class ProductSearchOutput
    {
        public int Id { get; set; }

        public int StoreId { get; set; }

        public int LocationId { get; set; }

        public int WarehouseId { get; set; }

        public string ProductName { get; set; }

        public string StoreCode { get; set; }

        public string StoreName { get; set; }

        public int? UnitType { get; set; }

        public int? ProductIdExternal { get; set; }

        public string GCode { get; set; }

        public string UnitTypeDescription { get; set; }

        public int? CategoryType { get; set; }

        public DateTime CreatedDate { get; set; }

        public string LocationName { get; set; }

        public int? LocationType { get; set; }

        public string ProductCode { get; set; }

        public string Description { get; set; }

        public int? Length { get; set; }

        public int? Width { get; set; }

        public int? Height { get; set; }

        public int? Weight { get; set; }

        public string Link { get; set; }
    }
}
