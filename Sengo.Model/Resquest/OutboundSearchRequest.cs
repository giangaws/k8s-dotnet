﻿namespace Sengo.Model.Resquest
{
    public class OutboundSearchRequest : BasePagingRequest
    {
        public string RefCode { get; set; }

        public string OutboundCode { get; set; }

        public int? LocationId { get; set; }

        public int? WarehouseId { get; set; }

        public int? LocationType { get; set; }

        public int? OutboundStatus { get; set; }

        public string StoreName { get; set; }

        public int? StoreId { get; set; }

        public string DateForCreate { get; set; }
    }
}