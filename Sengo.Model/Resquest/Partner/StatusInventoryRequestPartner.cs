﻿using System;

namespace Sengo.Model.Resquest.Partner
{
    public class StatusInventoryRequestPartner
    {
        public string SellerId { get; set; }

        public DateTime DateUpdate { get; set; }

        public int ETONLocationId { get; set; }

        public string RequestId { get; set; }

        public int ETONId { get; set; }

        public double Quantity { get; set; }

        public double AvailableQuantity { get; set; }

        public double OrderedQuantity { get; set; }
    }
}
