﻿using System.Collections.Generic;

namespace Sengo.Model.Resquest.Partner
{
    public class ModelsStatusOutboundResquestPartner
    {
        public string OrderId { get; set; }

        public List<ProductStatusOutboundResquestPartner> Products { get; set; }

        public string RequestId { get; set; }

        public string TrackingId { get; set; }

        public int PickingId { get; set; }

        public string PickingState { get; set; }

        public string Type { get; set; }
    }
}
