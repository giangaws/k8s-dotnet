﻿namespace Sengo.Model.Resquest.Partner
{
    public class ProductStatusOutboundResquestPartner
    {
        public string GCode { get; set; }

        public string ProductId { get; set; }

        public int Quantity { get; set; }
    }
}
