﻿namespace Sengo.Model.Resquest.Partner
{
    public class ProductInbound
    {
        public string GCode { get; set; }

        public string ProductId { get; set; }

        public double Quantity { get; set; }
    }
}
