﻿using System;

namespace Sengo.Model.Resquest.Partner
{
    public class InboundRequestPartner
    {
        public string RefCode { get; set; }

        public string Comment { get; set; }

        public string Status { get; set; }

        public DateTime DateUpdate { get; set; }
    }
}
