﻿using System;

namespace Sengo.Model.Resquest.Partner
{
    public class StatusInboundResquestPartner
    {
        public string RefCode { get; set; }

        public string Status { get; set; }

        public DateTime? DateUpdate { get; set; }

        public string Comment { get; set; }
    }
}
