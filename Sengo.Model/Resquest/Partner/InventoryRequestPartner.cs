﻿using System;

namespace Sengo.Model.Resquest.Partner
{
    public class InventoryRequestPartner 
    {
        public int? LocationId { get; set; }
        
        public string SCode { get; set; }
        
        public int? UnavailableQuantity { get; set; }

        public int? PhysicalQuantity { get; set; }

        public int? AvailableQuantity { get; set; }
        
        public string Status { get; set; } 

        public DateTime? DateUpdate { get; set; }

        public string Comment { get; set; }
    }
}
