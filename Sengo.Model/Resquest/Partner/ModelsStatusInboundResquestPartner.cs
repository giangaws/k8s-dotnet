﻿using System.Collections.Generic;

namespace Sengo.Model.Resquest.Partner
{
    public class ModelsStatusInboundResquestPartner
    {
        public string Origin { get; set; }

        public List<ProductInbound> Products { get; set; }

        public string RequestId { get; set; }

        public int PickingId { get; set; }

        public string PickingState { get; set; }

        public string Type { get; set; }

        public string DateUpdate { get; set; }
    }
}
