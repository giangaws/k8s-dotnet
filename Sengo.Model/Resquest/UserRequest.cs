﻿using Sengo.Model.Entities;
using System;
using System.ComponentModel.DataAnnotations;

namespace Sengo.Model.Resquest
{
    public class UserRequest : BaseEntity
    {
        [Required(ErrorMessage = "Vui lòng chọn nhóm quyền")]
        public int? RoleId { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập email")]
        //[EmailAddress(ErrorMessage = "Email không hợp lệ")]
        public string Email { get; set; }

        public string Phone { get; set; }

        public string FullName { get; set; }

        public int? Type { get; set; }

        public int? Status { get; set; }

        public string Avatar { get; set; }

        public DateTime? LastLoginDate { get; set; }

        public string Password { get; set; }

        public Guid? Salt { get; set; }
    }
}
