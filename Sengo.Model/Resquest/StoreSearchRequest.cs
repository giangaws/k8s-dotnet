﻿namespace Sengo.Model.Resquest
{
    public class StoreSearchRequest : BasePagingRequest
    {
        public string StoreName { get; set; }

        public string DateFull { get; set; }

        public string StoreCode { get; set; }
    }
}
