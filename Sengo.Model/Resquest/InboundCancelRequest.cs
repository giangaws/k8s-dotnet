﻿namespace Sengo.Model.Resquest
{
    public class InboundCancelRequest
    {
        public int Id { get; set; }

        public string RefCode { get; set; }
    }
}