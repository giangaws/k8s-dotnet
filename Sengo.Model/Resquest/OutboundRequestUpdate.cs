﻿namespace Sengo.Model.Resquest
{
    public class OutboundRequestUpdate
    {
        public int Id { get; set; }

        public string TrackingNumber { get; set; } 
    }
}
