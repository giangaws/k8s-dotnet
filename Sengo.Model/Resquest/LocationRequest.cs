﻿using Sengo.Model.Entities;

namespace Sengo.Model.Resquest
{
    public class LocationRequest : BaseEntity
    {
        public string LocationName { get; set; }

        public int? LocationType { get; set; }

        public string Address { get; set; }

        public int? WarehouseId { get; set; }
    }
}
