﻿namespace Sengo.Model.Resquest
{
    public class InventorySearchRequest : BasePagingRequest
    {
        public string ProductCode { get; set; }

        public string ProductName { get; set; }

        public int ProductExternalId { get; set; }

        public int CategoryType { get; set; }

        public int? UnavailableQuantityFrom { get; set; }

        public int? UnavailableQuantityTo { get; set; }

        public int? PhysicalQuantityFrom { get; set; }

        public int? PhysicalQuantityTo { get; set; }

        public int? AvailableQuantityFrom { get; set; }

        public int? AvailableQuantityTo { get; set; }

        public string StoreName { get; set; }

        public int? LocationId { get; set; }

        public int? LocationType { get; set; }

        public string GCode { get; set; }

        public string DateFull { get; set; }

        public int? WarehouseId { get; set; }

        public bool? IsActive { get; set; }

        public bool? IsSold { get; set; }

        public bool? IsStock { get; set; }
    }
}
