﻿using Sengo.Model.Entities;
using System;

namespace Sengo.Model.Resquest
{
    public class InventoryRequest : BaseEntity
    {
        public int? LocationId { get; set; }

        public int? StoreId { get; set; }

        public string StoreName { get; set; }

        public int? ProductId { get; set; }

        public string ProductCode { get; set; }

        public string ProductName { get; set; }

        public int? CategoryType { get; set; }

        public int? UnavailableQuantity { get; set; }

        public int? PhysicalQuantity { get; set; }

        public int? AvailableQuantity { get; set; }

        public int? Total { get; set; }

        public DateTime? DateUpdate { get; set; }

        public string Status { get; set; }

        public string RefStatus { get; set; }

        public DateTime? RefStatusDate { get; set; }

        public string Comment { get; set; }

        public int? ProductIdExternal { get; set; }

        public string GCode { get; set; }
    }
}
