﻿using System;

namespace Sengo.Model.Resquest
{
    public class StoreSearchOutput
    {
        public int Id { get; set; }

        public string StoreName { get; set; }

        public string StorePhone { get; set; }

        public string StoreCode { get; set; }

        public string Address { get; set; }

        public int CityId { get; set; }

        public string CityName { get; set; }

        public int DistrictId { get; set; }

        public string DistrictName { get; set; }

        public int WardId { get; set; }

        public string WardName { get; set; }

        public string PepresentName { get; set; }

        public string Description { get; set; }

        public string Email { get; set; }

        public string Slogan { get; set; }

        public string Website { get; set; }

        public string Logo { get; set; }

        public DateTime CreatedDate { get; set; }

        public string CreatedUser { get; set; }

        public string UpdatedUser { get; set; }

        public DateTime UpdatedDate { get; set; }

        public bool IsDeleted { get; set; }

        public byte[] VersionNo { get; set; }

        public string Deliveryer { get; set; }

        public string CMND { get; set; }

        public string AddressFull => Address + (WardName != null ? ", " + WardName : "") + (DistrictName != null ? ", " + DistrictName : "") + (CityName != null ? ", " + CityName : "");
    }
}
