﻿namespace Sengo.Model.Response
{
    public class StoreSendo
    {
        public bool Success { get; set; }

        public StoreSendoResult Result { get; set; }

        public Error Error { get; set; }
    }
}
