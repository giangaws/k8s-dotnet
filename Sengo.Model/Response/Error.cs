﻿namespace Sengo.Model.Response
{
    public class Error
    {
        public object Message { get; set; }

        public object Details { get; set; }
    }
}
