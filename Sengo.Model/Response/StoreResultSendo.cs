﻿namespace Sengo.Model.Response
{
    public class StoreSendoResult
    {
        public int Id { get; set; }

        public string UserName { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }

        public string ShopAddress { get; set; }

        public string ShopWard { get; set; }

        public int ShopDistrictId { get; set; }

        public int ShopRegionId { get; set; }

        public int ShopWardId { get; set; }

        public int WarehouseWardId { get; set; }

        public object ShopZipCode { get; set; }

        public string WarehouseAddress { get; set; }

        public string WarehouseWard { get; set; }

        public int WarehouseDistrictId { get; set; }

        public int WarehouseRegionId { get; set; }

        public object WarehouseZipCode { get; set; }

        public string Mobile { get; set; }

        public object Website { get; set; }

        public object ShopBackground { get; set; }

        public object ShopCover { get; set; }

        public string ShopLogo { get; set; }

        public string SystemURL { get; set; }

        public object Slogan { get; set; }

        public string ShopDescription { get; set; }

        public string ShopRepresentative { get; set; }

        public int ShopStatus { get; set; }

        public object ReasonComment { get; set; }

        public object Email { get; set; }

        public string LastLogin { get; set; }

        public object ReasonCode { get; set; }
    }
}
