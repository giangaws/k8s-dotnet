FROM mcr.microsoft.com/dotnet/core/sdk:2.2 AS build-env
COPY . /app
WORKDIR /app


# Copy csproj and restore as distinct layers
WORKDIR /app/Sengo.Email.Consumer

RUN dotnet restore

# Copy everything else and build
RUN dotnet publish -c Release -o out

# Build runtime image
FROM mcr.microsoft.com/dotnet/core/aspnet:2.2
WORKDIR /final
COPY --from=build-env /app/Sengo.Email.Consumer/out/ /final

RUN apt-get -y update \
    && apt-get -y install telnet vim net-tools \
    && rm -rf /var/lib/apt/lists/*

CMD ["dotnet","Sengo.Email.Consumer.dll","run"]

