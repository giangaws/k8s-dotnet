﻿using MassTransit.RabbitMqTransport;
using System;

namespace SengoCore.RabbitMQ
{
    public class QueueInfo
    {
        public Action<IRabbitMqReceiveEndpointConfigurator> SubscriptionAction { get; set; }

        public int Concurrency { get; set; }

        public string QueueUrl { get; set; }

        public string HandlerName { get; set; }
    }
}
