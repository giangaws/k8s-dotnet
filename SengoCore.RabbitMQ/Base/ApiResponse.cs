﻿namespace SengoCore.RabbitMQ.Base
{
    public class ApiResponse
    {
        public int Status { get; set; }

        public string Message { get; set; }

        public string Data { get; set; }
    }
}
