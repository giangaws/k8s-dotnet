﻿using log4net;
using SengoCore.RabbitMQ.Infrastructures;
using MassTransit;
using MassTransit.RabbitMqTransport;
using Microsoft.Extensions.Configuration;
using ServiceStack;
using ServiceStack.Text;
using System;
using System.Linq;

namespace SengoCore.RabbitMQ.Base
{
    public enum JsonType
    {
        Post,
        Get
    }

    public abstract class BaseHandler
    {
        protected ILog Logger;

        protected BaseHandler(Type inheritance)
        {
            Logger = LogManager.GetLogger(inheritance);
        }

        public abstract void GetConfigurator(IRabbitMqReceiveEndpointConfigurator configurator);

        public void HandlerJsonPost<TMessage>(ConsumeContext<TMessage> consume, string address, object value = null, bool checkResponse = true)
            where TMessage : class
        {
            HandlerJson(consume, address, JsonType.Post, value, checkResponse);
        }

        public void HandlerJsonGet<TMessage>(ConsumeContext<TMessage> consume, string address, object value = null, bool checkResponse = true)
            where TMessage : class
        {
            HandlerJson(consume, address, JsonType.Get, value, checkResponse);
        }

        private void HandlerJson<TMessage>(ConsumeContext<TMessage> consume, string address,
            JsonType type, object value = null, bool checkResponse = true)
            where TMessage : class
        {
            var response = string.Empty;
            var dumpMessage = consume.Message.Dump();
            var dumpValue = value.Dump();
            var queueName = consume.ReceiveContext.InputAddress.AbsolutePath;

            if (!string.IsNullOrEmpty(queueName))
            {
                queueName = queueName.Replace("/", "");
            }

            try
            {
                Logger.InfoFormat("Start handlerjson queuename : {0} message : {1} value : {2} ",
                    queueName, dumpMessage, dumpValue);

                if (type == JsonType.Get)
                {
                    response = address.GetJsonFromUrl(request =>
                    {
                        request.AutomaticDecompression = System.Net.DecompressionMethods.GZip | System.Net.DecompressionMethods.Deflate;
                    });
                }

                if (type == JsonType.Post)
                {
                    response = address.PostJsonToUrl(value.ToJson(), request =>
                    {
                        request.AutomaticDecompression = System.Net.DecompressionMethods.GZip | System.Net.DecompressionMethods.Deflate;
                    });
                }

                if (checkResponse)
                {
                    HandleJsonError(response, consume, () =>
                    {
                        Logger.InfoFormat("Success queuename : {0} message : {1} response : {2}",
                            queueName, dumpMessage, response.Dump());
                    }, s =>
                    {
                        Logger.ErrorFormat("Fail queuename : {0} errorMessage : {1} address : {2} message : {3} response : {4}",
                            queueName, s, address, dumpMessage, response.Dump());
                        MoveToError(consume);
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.FatalFormat("Exception queuename : {0} errorMessage : {1}  address : {2} - message : {3}", queueName, ex.Dump(), address, dumpMessage);
                MoveToError(consume);
            }
        }

        public void Handler<TMessage>(ConsumeContext<TMessage> context, Func<Response> action)
            where TMessage : class
        {
            var dumpMessage = context.Message.Dump();

            var queueName = context.ReceiveContext.InputAddress.AbsolutePath;
            if (!string.IsNullOrEmpty(queueName)) queueName = queueName.Replace("/", "");

            try
            {
                Logger.InfoFormat("Start handler queuemame : {0} message : {1}", queueName, dumpMessage);

                var response = action();

                if (response == null)
                {
                    Logger.ErrorFormat("Fail queuemame : {0}  errorMessage : {1}  message : {2}", queueName, "No response return", dumpMessage);
                    MoveToError(context);
                    return;
                }

                if (response.HasError)
                {
                    Logger.ErrorFormat("Fail queuemame : {0} errorMessage : {1}  message : {2} response : {3}",
                        queueName, response.ErrorMessage, dumpMessage, response.Dump());

                    MoveToError(context);
                }
                else
                {
                    Logger.InfoFormat("Success queuemame : {0} message : {1}  response: {2}",
                        queueName, dumpMessage, response.Dump());
                }
            }
            catch (Exception ex)
            {
                Logger.FatalFormat("Exception queuemame : {0} errorMessage : {1}  message : {2}", queueName, ex.Dump(), dumpMessage);
                MoveToError(context);
            }
        }

        protected void MoveToError<TMessage>(ConsumeContext<TMessage> context) where TMessage : class
        {
            var errUriStr = context.ReceiveContext.InputAddress.ToString();

            int? retryCount = 0;

            if (context.Headers.GetAll().Any())
            {
                retryCount = context.Headers.Get("RetryCount", default(int?));
            }

            if (retryCount >= 3)
            {
                if (!string.IsNullOrEmpty(errUriStr))
                {
                    errUriStr = errUriStr + "_ERROR_ERROR";
                }

                Logger.InfoFormat("Max retry {0} in queue {1} message :{2}", retryCount, errUriStr, context.Message.Dump());
            }
            else if (!string.IsNullOrEmpty(errUriStr))
            {
                errUriStr = errUriStr + "_ERROR";
            }

            retryCount++;
            context.GetSendEndpoint(new Uri(errUriStr)).Result.Send(context.Message, config =>
            {
                config.Headers.Set("RetryCount", retryCount);
            });
            // context.GetSendEndpoint(new Uri(errUriStr)).Result.Send(context.Message, config => config.Headers.Set("RetryCount", retryCount));
        }

        public virtual void HandleJsonError<TMessage>(string response, ConsumeContext<TMessage> consume, Action onSuccess, Action<string> onFail)
            where TMessage : class
        {
            var result = response.FromJson<ApiResponse>();

            if (response == null)
            {
                onFail(result.Message);
                return;
            }

            if (result.Status != 200)
            {
                onFail(result.Message);
            }
            else
            {
                onSuccess();
            }
        }

        public virtual void HandleError<TMessage>(object response, ConsumeContext<TMessage> consume,
            Action onSuccess, Action<string> onFail) where TMessage : class
        {
            if (response == null) onFail("response is null");
            else onSuccess();
        }
    }
}
