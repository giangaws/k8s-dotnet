﻿using MassTransit;
using System;

namespace SengoCore.RabbitMQ.Base
{
    public static class Routing
    {
        public static void SendData<T>(this IBusControl bus, T message, string uriName) where T : class
        {
            var uri = string.Format("rabbitmq://{0}/{1}", Init.BusControl.Address.Host, uriName);

            bus.GetSendEndpoint(new Uri(uri)).Result.Send(message, config => config.Headers.Set("RetryCount", 0));
        }
    }
}
