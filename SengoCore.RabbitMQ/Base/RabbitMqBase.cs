﻿using log4net;
using MassTransit;
using MassTransit.Log4NetIntegration;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;

namespace SengoCore.RabbitMQ.Base
{
    public class RabbitMqBase
    {
        protected ILog Logger;

        public RabbitMqBase(Type inheritance)
        {
            Logger = LogManager.GetLogger(inheritance);
        }

        public void StartServicePublish(IConfiguration configuration)
        {
            Init.BusControl = Bus.Factory.CreateUsingRabbitMq(sbc =>
            {
                sbc.Host(new Uri("rabbitmq://" + configuration.GetSection("AppSettings:RabbitMqServer").Value), h =>
                {
                    h.Username(configuration.GetSection("AppSettings:RabbitMqUserName").Value);
                    h.Password(configuration.GetSection("AppSettings:RabbitMqPassWord").Value);
                    h.PublisherConfirmation = false;
                });
                sbc.UseJsonSerializer();
                sbc.UseLog4Net();
            });
        }

        public void StartMultiServiceSubscriptionAction(List<QueueInfo> queueInfos, IConfiguration configuration)
        {
            Init.BusControl = Bus.Factory.CreateUsingRabbitMq(sbc =>
            {
                var host = sbc.Host(new Uri("rabbitmq://" + configuration.GetSection("AppSettings:RabbitMqServer").Value), h =>
                {
                    h.Username(configuration.GetSection("AppSettings:RabbitMqUserName").Value);
                    h.Password(configuration.GetSection("AppSettings:RabbitMqPassWord").Value);
                    h.PublisherConfirmation = false;
                    h.Heartbeat(5);
                });
                sbc.Durable = true;
                sbc.OverrideDefaultBusEndpointQueueName("FBS");
                sbc.SetExchangeArgument("x-expires", null);
                sbc.SetQueueArgument("x-expires", null);
                sbc.AutoDelete = false;
                sbc.UseJsonSerializer();
                sbc.UseLog4Net();

                foreach (var queueInfo in queueInfos)
                {
                    Logger.InfoFormat("Start queue {0} - concurrency {1}", queueInfo.QueueUrl, queueInfo.Concurrency);

                    sbc.ReceiveEndpoint(host, queueInfo.QueueUrl, queueInfo.SubscriptionAction);
                }
            });

            Init.BusControl.Start();
        }
    }
}
