﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace SengoCore.RabbitMQ.Libs
{
    public static class HandlerHelper
    {
        private const string LibraryFileExtension = "*.dll";

        public static List<Assembly> LoadAssemblies(string path)
        {
            var directory = new DirectoryInfo(path);
            var files = directory.GetFiles(LibraryFileExtension);

            return files.Select(item => Assembly.LoadFile(item.FullName)).ToList();
        }

        public static List<Type> GetHandlers(List<Assembly> assemblies, Type baseType)
        {
            var result = new List<Type>();
            foreach (var item in assemblies)
            {
                result.AddRange(item.GetTypes().Where(a => a.BaseType == baseType));
            }

            return result.OrderBy(a => a.Name).ToList();
        }
    }
}
