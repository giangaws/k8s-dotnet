﻿using SengoCore.RabbitMQ.Base;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SengoCore.RabbitMQ.Libs
{
    public static class Utils
    {
        private const string Prefix = "Sengo.";

        public static List<Type> GetHandlers()
        {
            var assemblies = AppDomain.CurrentDomain.GetAssemblies().Where(a => a.FullName.StartsWith(Prefix)).ToList();
            if (assemblies.Count > 0)
            {
                return HandlerHelper.GetHandlers(assemblies, typeof(BaseHandler));
            }

            return null;
        }
    }
}
