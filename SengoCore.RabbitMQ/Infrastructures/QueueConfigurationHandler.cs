﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Xml;

namespace SengoCore.RabbitMQ.Infrastructures
{
    public class QueueConfigurationHandler : IConfigurationSectionHandler
    {
        public object Create(object parent, object configContext, XmlNode section)
        {
            var items = new List<QueueInfo>();
            var processesNodes = section.SelectNodes("queue");

            if (processesNodes != null)
            {
                items.AddRange(from XmlNode processNode in processesNodes
                               let xmlAttributeCollection = processNode.Attributes
                               where xmlAttributeCollection != null
                               select new QueueInfo
                               {
                                   QueueUrl = xmlAttributeCollection["queueUrl"].InnerText,
                                   Concurrency = int.Parse(xmlAttributeCollection["concurrency"].InnerText),
                                   HandlerName = xmlAttributeCollection["handlerName"].InnerText,
                               });
            }

            return items;
        }
    }
}
