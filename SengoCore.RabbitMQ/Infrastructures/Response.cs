﻿namespace SengoCore.RabbitMQ.Infrastructures
{
    public class Response<T> : Response
    {
        public T Data { get; set; }

        public bool SuccessData()
        {
            return HasError == false && Data != null;
        }

        public Response<T> Fail(string errorMessage)
        {
            return new Response<T>
            {
                HasError = true,
                ErrorMessage = errorMessage
            };
        }

        public Response<T> Success(T data)
        {
            return new Response<T>
            {
                HasError = false,
                Data = data
            };
        }
    }

    public class Response
    {
        public bool HasError { get; set; }

        public bool IsRetry { get; set; }

        public string ErrorMessage { get; set; }

        public static Response Fail(string errorMessage, bool isRetry)
        {
            return new Response
            {
                HasError = true,
                IsRetry = isRetry,
                ErrorMessage = errorMessage
            };
        }

        public static Response Success()
        {
            return new Response
            {
                HasError = false,
            };
        }
    }
}
