﻿using log4net;
using MassTransit;
using MassTransit.Log4NetIntegration.Logging;
using Microsoft.Extensions.Configuration;
using SengoCore.RabbitMQ.Base;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SengoCore.RabbitMQ
{
    public static class Init
    {
        public static IBusControl BusControl { get; set; }
        public static RabbitMqBase RabbitMqBase { get; set; }
        public static ILog Logger = LogManager.GetLogger(typeof(Init));

        static Init()
        {
            ConfigLog();
            RabbitMqBase = new RabbitMqBase(typeof(RabbitMqBase));
        }

        public static void StartServicePublish(IConfiguration section)
        {
            RabbitMqBase.StartServicePublish(section);

            BusControl.Start();
        }

        public static void StartWebApiMultiSubscriptionAction(List<Type> handler, IConfiguration section)
        {
            var queueInfos = ReadSectiondQueueInfo(section);

            foreach (var queueInfo in queueInfos)
            {
                var instance = (BaseHandler)Activator.CreateInstance(handler.Single(h => h.Name == queueInfo.HandlerName));
                queueInfo.SubscriptionAction = instance.GetConfigurator;
            }
            RabbitMqBase.StartMultiServiceSubscriptionAction(queueInfos, section);
        }

        private static void ConfigLog()
        {
            //log4net.Config.XmlConfigurator.Configure(); before upgrade to .Netcore
            Log4NetLogger.Use();
        }

        public static List<QueueInfo> ReadSectiondQueueInfo(IConfiguration section)
        {

            var list = new List<QueueInfo>();
            var item = new QueueInfo
            {
                QueueUrl = "SENGO_SENDMAIL",
                Concurrency = 4,
                HandlerName = "MailHandler"
            };
            list.Add(item);
            return list;
        }
    }
}
