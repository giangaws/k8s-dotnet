﻿using System;
using System.Security.Cryptography;

namespace Sengo.Utils
{
    public static class Algorithm
    {
        //B1
        public static string SHA256ByString(string text)
        {
            using (SHA256 hasher = SHA256.Create())
            {
                var encoding = new System.Text.UTF8Encoding();
                byte[] keyByte = encoding.GetBytes(text);
                byte[] hashValue = hasher.ComputeHash(keyByte);

                return Convert.ToBase64String(hashValue);
            }
        }

        //B2
        public static string HMAC(string message, string secret)
        {
            secret = secret ?? "";
            var encoding = new System.Text.ASCIIEncoding();
            byte[] keyByte = encoding.GetBytes(secret);
            byte[] messageBytes = encoding.GetBytes(message);

            using (var hmacsha256 = new HMACSHA256(keyByte))
            {
                byte[] hashmessage = hmacsha256.ComputeHash(messageBytes);

                return Convert.ToBase64String(hashmessage);
            }
        }

        //B:0
        public static string CreateHMACDigest(string partner_client_secret, string method, string path, string mime, string body, string dateutc)
        {
            string content_digest;
            if (!string.IsNullOrEmpty(body))
            {
                //Buoc 1
                content_digest = SHA256ByString(body);
            }
            else
            {
                content_digest = "";
                //mime = "";
            }
            var string_to_sign =
            method.ToUpper() + "\n" + mime +
            "\n" +
            dateutc +
            "\n" +
            path +
            "\n" +
            content_digest +
            "\n";

            //Buoc 2
            return HMAC(string_to_sign, partner_client_secret);//đảo lại
        }
    }
}