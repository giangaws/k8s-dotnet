﻿using System;

namespace Sengo.Utils.UICommon
{
    public class Paging
    {
        public int TotalItems { get; private set; }

        public int CurrentPage { get; private set; }

        public int PageSize { get; private set; }

        public int TotalPages { get; private set; }

        public int StartPage { get; private set; }

        public int EndPage { get; private set; }

        public string Action { get; set; }

        public int ItemFrom { get; private set; }

        public int ItemTo { get; private set; }

        public Paging(int totalItems, int? page, int pageSize = 12, int startpage = 5, int endpage = 4)
        {
            // calculate total, start and end pages
            var totalPages = (int)Math.Ceiling((decimal)totalItems / (decimal)pageSize);
            var currentPage = page != null ? (int)page : 1;
            var startPage = currentPage - startpage;
            var endPage = currentPage + endpage;

            if (startPage <= 0)
            {
                endPage -= (startPage - 1);
                startPage = 1;
            }

            if (endPage > totalPages)
            {
                endPage = totalPages;

                if (endPage > 10)
                {
                    startPage = endPage - 9;
                }
            }

            TotalItems = totalItems;
            CurrentPage = currentPage;
            PageSize = pageSize;
            TotalPages = totalPages;
            StartPage = startPage;
            EndPage = endPage;
            ItemFrom = pageSize * (currentPage - 1) + 1;
            ItemTo = currentPage != endPage ? pageSize * currentPage : totalItems;
        }
    }
}
