﻿using log4net;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace Sengo.Utils.UICommon
{
    public static class Common
    {
        public static ILog log = LogManager.GetLogger
        (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static async Task<T> GetJson<T>(this string requestAddress)
        {
            try
            {
                log.Info("[GetJson] requestAddress: " + requestAddress);
                //Setup request
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(new Uri($"{requestAddress}"));
                httpWebRequest.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
                httpWebRequest.Accept = "application/json";
                httpWebRequest.ContentType = "application/json; charset=utf-8";
                httpWebRequest.Method = WebRequestMethods.Http.Get;

                dynamic result;
                var httpResponse = await httpWebRequest.GetResponseAsync();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = await streamReader.ReadToEndAsync();
                }

                var rs = JsonConvert.DeserializeObject<T>(result);
                log.Info("[GetJson] response: " + requestAddress + ",value: " + JsonConvert.SerializeObject(rs));

                return rs;
            }
            catch (Exception ex)
            {
                log.Info("GetJsonEx requestAddress: " + requestAddress + ",Ex: " + ex.Message);

                return default;
            }
        }

        public static async Task<T> PostJson<T>(this string requestAddress, object value)
        {
            try
            {
                //Setup requestk
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(new Uri(requestAddress));
                httpWebRequest.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
                httpWebRequest.Accept = "application/json";
                httpWebRequest.ContentType = "application/json; charset=utf-8";
                //httpWebRequest.Headers.Add("Authorization", "7bf32951c90e4645a18f127d5a95bc0a");
                httpWebRequest.Method = WebRequestMethods.Http.Post;

                dynamic result;
                using (var streamWriter = new StreamWriter(await httpWebRequest.GetRequestStreamAsync()))
                {
                    if (value != null)
                    {
                        streamWriter.Write(JsonConvert.SerializeObject(value));
                    }
                    await streamWriter.FlushAsync();
                    streamWriter.Close();

                    var httpResponse = await httpWebRequest.GetResponseAsync();
                    using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                    {
                        result = await streamReader.ReadToEndAsync();
                    }
                }
                var rs = JsonConvert.DeserializeObject<T>(result);
                log.Info("[PostJson] response: " + requestAddress + ",value: " + JsonConvert.SerializeObject(rs) + " , object:" + JsonConvert.SerializeObject(value));

                return rs;
            }
            catch (Exception ex)
            {
                log.Info("PostJsonex response: " + ex.Message);

                return default;
            }
        }
    }
}
