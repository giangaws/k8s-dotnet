﻿using System;
using System.Collections.Generic;

namespace Sengo.Utils.UICommon
{
    public class PagerSearch<T> where T : class
    {
        public List<T> Results { get; set; }

        public int TotalRecords { get; set; }

        public int TotalPages { get; set; }

        public int Page { get; set; }

        public int DislayFrom { get; set; }

        public int DislayTo { get; set; }

        public bool? IsSendo { get; set; }

        public PagerSearch(int totalItems, int pageSize, int totalRecords, int pageNumber)
        {
            Results = new List<T>();
            TotalPages = (int)Math.Ceiling((decimal)totalItems / (decimal)pageSize);
            TotalRecords = totalRecords;
            DislayFrom = (pageNumber - 1) * pageSize + 1;
            DislayTo = pageNumber * 10 > TotalRecords ? TotalRecords : pageNumber * 10;
        }
    }
}
