﻿using Sengo.Model.Entities;
using Sengo.Model.Resquest;
using System;
using System.Linq;

namespace Sengo.Utils.Session
{
    public class SessionHelper
    {
        #region User
        public UserOutput CurrentUser
        {
            get
            {
                return (UserOutput)Get(SessionKey.CurrentUser);
            }
            set
            {
                Set(SessionKey.CurrentUser, value);
            }
        }

        public UserOutput Session_Get()
        {
            //var keyCache = Cookies_Get("__tksen_go");
            var user = Get(SessionKey.CurrentUser);
            if (user != null)
            {
                return (UserOutput)user;
            }

            return null;
        }

        public bool Session_Set(UserOutput user)
        {
            string value = "_sg_sessiion_" + Guid.NewGuid().ToString().Substring(0, 8);
            //Cookies_Set("__tksen_go", value, 60);
            Set(SessionKey.CurrentUser, user);

            return true;
        }

        public bool IsInRole(string feature)
        {
            if (CurrentUser.RoleFeatures.Any(r => feature.Split(',').Contains(r.Feature.ToString()) && r.Enable))
            {
                return true;
            }

            return false;
        }
        #endregion User

        #region Warehouse
        public Warehouses CurrentWarehouse
        {
            get
            {
                return (Warehouses)Get(SessionKey.CurrentWarehouse);
            }
            set
            {
                Set(SessionKey.CurrentWarehouse, value);
            }
        }

        public Warehouses Session_GetWarehouse()
        {
            var warehouse = Get(SessionKey.CurrentWarehouse);
            if (warehouse != null)
            {
                return (Warehouses)warehouse;
            }

            return null;
        }

        public bool Session_SetWarehouse(Warehouses warehouse)
        {
            //string value = "_sg_sessiion_" + Guid.NewGuid().ToString().Substring(0, 8);
            //Cookies_Set("__tksen_go", value, 60);
            Set(SessionKey.CurrentWarehouse, warehouse);

            return true;
        }
        #endregion Warehouse

        public string Cookies_Get(string key)
        {
            string value = "";
            //if (HttpContext.Current.Request.Cookies.AllKeys.Contains(key))
            //{
            //    value = HttpContext.Current.Request.Cookies[key].Value;
            //}

            return value;
        }

        public void Cookies_Set(string key, string value, int expiresMinute)
        {
            //HttpCookie cookie = new HttpCookie(key, value);
            //cookie.Expires = DateTime.Now.AddMinutes(30);
            //HttpContext.Current.Response.SetCookie(cookie);
        }

        public object Get(string key)
        {
            return null;// Session[key];
        }

        public void Set(string key, object source)
        {
            //Session[key] = source;
        }

        public void Remove(string key)
        {
            //Session.Remove(key);
        }

        public void Abandon()
        {
            //Session.Clear();
            //Session.Abandon();
        }

        public void Cookies_Remove(string key)
        {
            //if (HttpContext.Current.Request.Cookies[key] != null)
            //{
            //    var c = new HttpCookie(key);
            //    c.Expires = DateTime.Now.AddDays(-1);
            //    HttpContext.Current.Response.Cookies.Add(c);
            //}
        }
        //protected HttpSessionState Session => HttpContext.Current.Session;
    }
}
