﻿namespace Sengo.Utils.Session
{
    public static class SessionKey
    {
        public const string CurrentUser = "__Fullfillment_Current_User";
        public const string CurrentWarehouse = "__Fullfillment_warehouse";
    }
}
