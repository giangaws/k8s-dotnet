﻿using Inside.Sengo.Models.Schedule;
using MassTransit;
using MassTransit.RabbitMqTransport;
using Sengo.Model.BaseReponse;
using Sengo.Model.Type;
using Sengo.Utils;
using SengoCore.RabbitMQ.Base;
using SengoCore.RabbitMQ.Infrastructures;
using ServiceStack;
using ServiceStack.Text;
using System;
using System.Net;
using System.Threading.Tasks;

namespace Email.Sengo.Consumer.Handler
{
    public class MailHandler : BaseHandler, IConsumer<SchedulerRequest>
    {
        private readonly string _apiUrl;

        public MailHandler() : base(typeof(MailHandler))
        {
            _apiUrl = StaticConfigs.GetConfig("ApiInside");
        }

        public Task Consume(ConsumeContext<SchedulerRequest> context)
        {
            var data = context.Message;
            Console.WriteLine("ScheduleHandler consumer data:" + data.ToJson());
            Console.WriteLine("ScheduleHandler ScheduleAction:" + (int)data.ScheduleAction);
            Console.WriteLine("ScheduleHandler RetryQueue:" + (int)ScheduleAction.RetryQueue);
            Console.WriteLine("_apiUrl " + _apiUrl);
            Logger.Info("_apiUrl " + _apiUrl);

            if ((int)data.ScheduleAction == (int)ScheduleAction.RetryQueue)
            {
                Console.WriteLine("ProcessRetryQueue " + data.ToJson());
                ProcessRetryQueue(context);
            }

            Console.WriteLine("Consume Notfound " + data.ToJson());
            return Task.FromResult(0);
        }

        public Task ProcessRetryQueue(ConsumeContext<SchedulerRequest> context)
        {
            var data = context.Message;
            Handler(context, () =>
            {
                var currentApi = $"{_apiUrl}/api/retry/all";
                try
                {
                    Console.WriteLine("ProcessRetryQueue " + data.ToJson() + " , url" + currentApi);
                    var json = currentApi.PostStringToUrl(data.ToJson(), requestFilter: request => request.Timeout = 10000, contentType: "application/json");
                    var rsl = json.FromJson<IntegrationResponse<string>>();
                    if (rsl.Status == HttpStatusCode.OK)
                    {
                        Console.WriteLine("ProcessRetryQueue Successful");
                        return Response.Success();
                    }
                    return Response.Success();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("ProcessRetryQueue ex :" + ex.Message + " ,url" + currentApi + " ,data:" + data.ToJson());
                    Logger.FatalFormat("{0} - ProcessRetryQueue url: {1} - ex: {2}", data.Dump(), currentApi, ex.Dump());
                    return Response.Fail(ex.Message, false);
                }
            });

            return Task.FromResult(0);
        }

        public override void GetConfigurator(IRabbitMqReceiveEndpointConfigurator configurator)
        {
            configurator.Consumer<MailHandler>();
            configurator.PrefetchCount = 8;
        }
    }
}

namespace Inside.Sengo.Models.Schedule
{
    public class SchedulerRequest
    {
        public ScheduleAction ScheduleAction { get; set; }
        public Guid Guid { get; set; }
    }
}

