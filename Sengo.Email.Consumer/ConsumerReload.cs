﻿using Microsoft.Extensions.Configuration;

namespace Email.Sengo.Consumer
{
    public class ConsumerReload
    {
        public class RabbitmqInstance
        {
            public static void Start(IConfiguration section)
            {
                var handlers = SengoCore.RabbitMQ.Libs.Utils.GetHandlers();
                SengoCore.RabbitMQ.Init.StartWebApiMultiSubscriptionAction(handlers, section);
            }
        }
    }
}
