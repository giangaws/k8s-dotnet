﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using NLog.Web;
using Sengo.Email.Consumer;
using ServiceStack;
using Microsoft.Extensions.DependencyInjection;
using ServiceStack.Text;

namespace Email.Sengo.Consumer
{
    public class Program
    {
        public static void Main(string[] args)
        {
            try
            {
                var serviceProvider = ContainerConfiguration.Configure();
                var logger = serviceProvider.GetService<ILogger<Program>>();
                logger.LogInformation("Sengo SengoEmailConsumer Template {message}", "Init APIInsideSengo");
                CreateWebHostBuilder(args).Build().Run();
            }
            catch (Exception exception)
            {
                Console.WriteLine("Sengo APIInsideSengo MainErr :" + exception.Dump());
                throw;
            }
            finally
            {
                NLog.LogManager.Shutdown();
            }
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
             .ConfigureAppConfiguration((builderContext, config) =>
             {
                 IHostingEnvironment env = builderContext.HostingEnvironment;

                 config.AddJsonFile("appsettings.json", optional: false, reloadOnChange: false);
             })
             .UseStartup<Startup>();


    }
}
