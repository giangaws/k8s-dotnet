﻿using MassTransit;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Email.Sengo.Consumer.Handler;
using ServiceStack.Text;
using static Email.Sengo.Consumer.ConsumerReload;

namespace Email.Sengo.Consumer
{
    public class Startup
    {
        public static IConfiguration StaticConfig { get; private set; }

        public Startup(IConfiguration configuration)
        {
            JsConfig.DateHandler = DateHandler.ISO8601;
            JsConfig.IncludePublicFields = true;
            StaticConfig = configuration;
            RabbitmqInstance.Start(configuration);
            SengoCore.RabbitMQ.Init.StartServicePublish(configuration);
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IConsumer,MailHandler>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.Run(async (context) =>
            {
                await context.Response.WriteAsync("Start consumer");
            });
        }
    }
}
